<div class="wrap">
	<div class="icon32" id="icon-options-general"><br></div>
	<h2>Apidae Structure</h2>
	<?php
		// Check for compatibility
		$sbcdErrMess = null;
		global $wp_version;
		if ( version_compare($wp_version, '4.4', '<') ) {
			$sbcdErrMess .= __('- La version de Wordpress doit être supérieure ou égale à 4.4.');
		};
		if (!extension_loaded('zip')) {
			if ($sbcdErrMess != null) {
				$sbcdErrMess .= '<br />';
			}
			$sbcdErrMess = __('- L\'extension ZIP doit être activée dans PHP.');
		}
		if (!extension_loaded('xsl')) {
			if ($sbcdErrMess != null) {
				$sbcdErrMess .= '<br />';
			}
			$sbcdErrMess .= __('- L\'extension XSLT doit être activée dans PHP.');
		}
		if ($sbcdErrMess != null) {
			echo '<div class="updated error notice is-dismissible">';
			echo '	<h3><strong>'.__('Éléments manquants à l\'installation de cette extension').'</strong></h3>';
			echo '	<p><strong>'.$sbcdErrMess.'</strong></p>';
			echo '	<button class="notice-dismiss" type="button">'.
				 '		<span class="screen-reader-text">Ne pas tenir compte de ce message.</span>'.
				 '	</button>';
			echo '</div><br />';
		}
	?>

	<h3>Ce plugin offre les possibilités suivantes</h3>
	<ul>
		<li>- Recevoir et décompresser les données d'un export Apidae au format JSON V2,</li>
		<li>- Construire une représentation de la structure des données Apidae en croisant les schemas et le référentiel,</li>
		<li>- Rendre disponible la structure des données au téléchargement en XML ou CSV,</li>
		<li>- Ouvrir des API en lecture JSON pour les référentiels communes, territoires et éléments de référence.</li>
	</ul>

	<br />
	<h3>Réglages</h3>
	<form method="post" action="options.php">
		<?php
			settings_fields('sbcd_as_settings');
			do_settings_fields('sbcd-as-settings','sbcd_as_options');
			do_settings_fields('sbcd-as-settings','sbcd_as_api_activation');
		?>
		<p>L'<strong>Adresse des schémas JSON</strong> doit être l'URL de l'archive des derniers <a href="http://dev.apidae-tourisme.com/fr/documentation-technique/outils-developpeurs/schemas-json" target="_blank">schémas disponibles</a> pour l'<strong>API V2 en JSON</strong>.<br />
			L'URL a généralement la forme suivante : <code>http://dev.apidae-tourisme.com/wp-content/uploads/2017/11/2017-11-20-apidae-schemas-api-v2.zip</code>
		</p>
		<label for="sbcd_as_schema_path">URL des schémas API V2 :</label>
		<input name="sbcd_as_schema_path" type="text" id="sbcd_as_schema_path" value="<?php echo get_option('sbcd_as_schema_path'); ?>" />
		<?php
			// Si le schéma est déjà présent, on peut l'importer
			if (get_option('sbcd_as_schema_path') !='') { ?>
				<a href="<?php echo add_query_arg( 'sbcd_as_act', 'importSchema' ); ?>" title="Importer le schéma" class="button button-primary">Importer le schéma</a><?php
			} ?>
		<br /><br />
		<p><strong>Utilisation des API</strong>, les API permettent de renvoyer au format JSON V2 les référentiels communes, territoires et les éléments de référence. Les appels sont les suivants :</p>
		<ul>
			<li>- <strong>Référentiel communes</strong> : <code><?php echo esc_url( home_url( '/wp-json/sbcd-as/v1/api/communes' ) ); ?></code></li>
			<li>- <strong>Référentiel territoires</strong> : <code><?php echo esc_url( home_url( '/wp-json/sbcd-as/v1/api/territoires' ) ); ?></code></li>
			<li>- <strong>Éléments de référence</strong> : <code><?php echo esc_url( home_url( '/wp-json/sbcd-as/v1/api/referentiel' ) ); ?></code></li>
		</ul>
		<input name="sbcd_as_api_activation" type="checkbox" id="sbcd_as_api_activation" value="1" <?php checked('1', get_option('sbcd_as_api_activation')); ?> />
		<label for="sbcd_as_api_activation">Ouvrir les API de lecture du référentiel.</label>
		<?php
		submit_button();
		?>
	</form>

	<br/>
	<h3>ShortCode</h3>

	<p>Le shortcode suivant peut être utilisé dans des articles ou des pages, il permet d'afficher la structure des exports et des API de manière dynamique.</p>
	<p><code>[sbcd_structure]</code></p>

	<br />
	<h3>Projet Apidae</h3>
	<p>Un projet Apidae est nécessaire pour alimenter ce plugin. Le projet doit être configuré de la manière suivante :</p>
	<ul>
		<li><strong>- Technologies utilisées : </strong>Exports</li>
		<li><strong>- Format : </strong>JSON</li>
		<li><strong>- Version du format de sortie : </strong>V2</li>
		<li><strong>- Url de notification : </strong><code><?php echo esc_url( home_url( '/wp-json/sbcd-as/v1/export/notification' ) ); ?></code></li>
		<li><strong>- Adresses IP : </strong><?php echo $_SERVER['SERVER_ADDR']; ?></li>
	</ul>
</div>