<?php
/*
Plugin Name: Apidae Structure
Description: Construit une représentation de la structure des données Apidae (API et Exports V2) en croisant les schemas et le référentiel. Ce plugin permet aussi de traiter des exports JSON V2 et de mettre en place des API de diffusion des référentiels JSON. Le paramétrage du plugin s'effectue dans Réglages > Apidae Structure.
Version: 1.1
Author: Serge Bregliano - Apidae
Author URI: http://www.apidae-tourisme.com
*/

// Inclusion des fonctions outils.
require_once 'sbcd_apidae/sbcd_tools.php';

// ***** Intégration dans Wordpress *****

// Fonction de d'activation du plugin
register_activation_hook(__FILE__,'sbcd_as_activate');
// Fonction de désactivation du plugin
register_deactivation_hook(__FILE__,'sbcd_as_deactivate');
// Récupération des options stockées en base.

add_action('admin_init', 'sbcd_as_options');
// Administration du plugin
add_action( 'admin_menu', 'sbcd_as_menu' );
// Ajout des routes API et des notifications d'export
add_action( 'rest_api_init','sbcd_as_api');
// Ajout de l'action CRON pour traitement des exports Apidae (définit en activation)
add_action( 'apidae_check_export','sbcd_as_cron_export' );

// Ajout du ShortCode
add_shortcode('sbcd_structure','sbcd_run_structure');

function enqueue_scripts() {
	// Chargement du CSS lié au shortcode
	wp_register_style( 'sbcd-as-style', plugins_url('ui/sbcd_as.css', __FILE__) );
	wp_enqueue_style( 'sbcd-as-style' );
	// Chargement du JS lié au shortcode
	wp_enqueue_script( 'jquery' );
	wp_register_script( 'sbcd-as-script', plugins_url('ui/sbcd_as.js', __FILE__) );
	wp_enqueue_script( 'sbcd-as-script' );
}
add_action( 'admin_enqueue_scripts', 'enqueue_scripts' );
add_action( 'wp_enqueue_scripts', 'enqueue_scripts' );


// ***** Gestion de la structure Apidae *****

// Mise à jour du shéma en cas d'appel du bouton correspondant dans l'Admin
if (isset($_GET['sbcd_as_act']) && ($_GET['sbcd_as_act'] == 'importSchema')) {
	sbcd_as_upload_schema();
	$_SERVER['REQUEST_URI'] = remove_query_arg( 'sbcd_as_act');
}



// ***** Fonctions de gestion de la structure Apidae *****

/**
 * Charge le schéma mentionné en URL dans le dossier sbcd_as/schemas/.
 *
 * @access public
 * @return void
 */
function sbcd_as_upload_schema() {
	if ( $sbcd_as_schema_path = get_option('sbcd_as_schema_path') ) {
		$schemaDestination = WP_CONTENT_DIR.'/uploads/sbcd_as/schemas.zip';
		// Téléchargement des schémas
		$tmp = sbcd_tools::downloadFile($sbcd_as_schema_path,$schemaDestination);
		// Décompression des schémas
		if (!$zip = sbcd_tools::unzip($schemaDestination,WP_CONTENT_DIR.'/uploads/sbcd_as/schemas/')) {
	    	add_action( 'admin_notices', 'sbcd_as_notice_ZipArchive_not_present' );
			return false;
		}
  		// Effacement du fichier ZIP téléchargé
        @unlink($schemaDestination);
    	add_action( 'admin_notices', 'sbcd_as_notice_schema_updated' );
	}
}

/**
 * Affiche la notification d'import effectué.
 *
 * @access public
 * @return void
 */
function sbcd_as_notice_schema_updated() {
	echo '<div class="updated settings-error notice is-dismissible">';
	echo '	<p><strong>'.__('Import du schéma effectué.').'</strong></p>';
	echo '	<button class="notice-dismiss" type="button">'.
		 '		<span class="screen-reader-text">Ne pas tenir compte de ce message.</span>'.
		 '	</button>';
	echo '</div>';
}

/**
 * Affiche la notification de ZipArchiveManquant.
 *
 * @access public
 * @return void
 */
function sbcd_as_notice_ZipArchive_not_present() {
	echo '<div class="updated error notice is-dismissible">';
	echo '	<p><strong>'.__('Echec de l\'import. L\'extension ZIP doit être activée dans PHP.').'</strong></p>';
	echo '	<button class="notice-dismiss" type="button">'.
		 '		<span class="screen-reader-text">Ne pas tenir compte de ce message.</span>'.
		 '	</button>';
	echo '</div>';
}

/**
 * Renvoie la liste des types objets en HTML.
 *
 * @access public
 * @return string HTML
 */
function sbcd_as_listTypes() {
	require_once 'sbcd_apidae/sbcd_structure.php';
	// Affichage de la liste des types objets
	$typesObj = sbcd_structure::listTypesObjets();
	uksort($typesObj, "strnatcmp");

	$liste = '<div class="sbcd_as"><ul>';
	foreach ($typesObj as $typeName => $typeLink) {
		$liste .= '<li><a href="'.add_query_arg( 'sbcd_as_sch', $typeLink ).'" alt="'.$typeName.'">'.$typeName.'</a></li>';
	}
	$liste .= '<ul></div>';
	return $liste;
}

/**
 * Permet de télécharger le fichier de structure au format CSV.
 *
 * @access public
 * @return file download
 */
function sbcd_as_downloadCSV() {
	// Appel de la structure
	require_once 'sbcd_apidae/sbcd_structure.php';
	require_once 'sbcd_apidae/sbcd_cache.php';
	$name = $_GET['sbcd_as_sch'];
	$schemaPath = WP_CONTENT_DIR.'/uploads/sbcd_as/schemas/';
	$referencePath = WP_CONTENT_DIR.'/uploads/sbcd_as/export/';
	$cachePath = WP_CONTENT_DIR.'/uploads/sbcd_as/structure/';
	$struct = new sbcd_structure($name,$schemaPath,$referencePath,$cachePath);

	// Renvoi du fichier
	header("content-type:application/csv;charset=UTF-8");
	header("Content-disposition: attachment; filename=".strtolower($name).".csv");
	header("Pragma: no-cache");
	header("Expires: 0");
	$fp = fopen('php://output', 'w');
	$enteteCsv = array('Ligne','Objet','Chemin','Type Objet','Actif','Code',utf8_decode('Français'),'Anglais', 'Espagnol', 'Italien', 'Allemand', utf8_decode('Néerlandais'));
	fputcsv($fp, $enteteCsv,';');
	$struct->parseXML($struct->structure, $fp, $struct);
	fclose($fp);
	exit;
}

/**
 * Permet de télécharger le fichier de structure au format XML.
 *
 * @access public
 * @return file download
 */
function sbcd_as_downloadXML() {
	// Appel de la structure
	require_once 'sbcd_apidae/sbcd_structure.php';
	require_once 'sbcd_apidae/sbcd_cache.php';
	$name = $_GET['sbcd_as_sch'];
	$schemaPath = WP_CONTENT_DIR.'/uploads/sbcd_as/schemas/';
	$referencePath = WP_CONTENT_DIR.'/uploads/sbcd_as/export/';
	$cachePath = WP_CONTENT_DIR.'/uploads/sbcd_as/structure/';
	$struct = new sbcd_structure($name,$schemaPath,$referencePath,$cachePath);

	// Renvoi du fichier
	header('Content-Type: application/xml');
	header('Content-type: text/xml; charset=utf-8');
	header('Cache-Control: no-cache, must-revalidate');
	header('Content-disposition: attachment; filename='.strtolower($name).'.xml');
	$content = $struct->structure->asXML();
	$xml = fopen('php://output', 'w');
	fwrite($xml, $content);
	fclose($xml);
	exit;
}


// ***** Fonctions de gestion des exports *****

/**
 * Appelle la notification d'export. Provoque l'enregistrement du fichier de notification
 *
 * @access public
 * @return void
 */
function sbcd_as_setNotification() {
	require_once 'sbcd_apidae/sbcd_export.php';

	$xPort = new sbcd_export(WP_CONTENT_DIR.'/uploads/sbcd_as/');
	return $xPort->getNotification();
}

/**
 * Vérifie toutes les heures si un nouvel export est disponible.
 *
 * @access public
 * @return bool
 */
function sbcd_as_cron_export() {
	require_once 'sbcd_apidae/sbcd_export.php';

	$xPort = new sbcd_export(WP_CONTENT_DIR.'/uploads/sbcd_as/');
	return $xPort->cronWalk();
}


// ***** Fonctions d'intégration à Wordpress *****

/**
 * Activation du plugin.
 *
 * @access public
 * @return void
 */
function sbcd_as_activate() {
	// Placement des valeurs par défaut
	$sbcd_as_schema_path = get_option('sbcd_as_schema_path');
	// Création du dossier temporaire s'il n'existe pas déjà
	$baseFolder = WP_CONTENT_DIR.'/uploads/sbcd_as/';

	sbcd_tools::safe_mkdir($baseFolder);
	sbcd_tools::safe_mkdir($baseFolder.'schemas/');
	sbcd_tools::safe_mkdir($baseFolder.'export/');
	sbcd_tools::safe_mkdir($baseFolder.'structure/');
	// Ajout du CRON pour l'import
    if (! wp_next_scheduled ( 'apidae_check_export' )) {
		wp_schedule_event(time(), 'hourly', 'apidae_check_export');
    }
}

/**
 * Désactivation du plugin et effacement des paramètres.
 *
 * @access public
 * @return true
 */
function sbcd_as_deactivate() {
	delete_option('sbcd_as_schema_path');
	delete_option('sbcd_as_api_activation');
	// Supression du CRON
	wp_clear_scheduled_hook('apidae_check_export');
	// Effacement du dossier temporaire
	if (is_dir(WP_CONTENT_DIR.'/uploads/sbcd_as/')) {
		if (!sbcd_tools::safe_rmdir(WP_CONTENT_DIR.'/uploads/sbcd_as/')) {
			wp_die( sprintf(__( 'The temporary folder cannot be deleted. You will have to delete %s manually.'),WP_CONTENT_DIR.'/uploads/sbcd_as/') );
		}
	}
	return true;
}

/**
 * Déclaration des données stockées en base.
 *
 * @access public
 * @return void
 */
function sbcd_as_options() {
	register_setting('sbcd_as_settings', 'sbcd_as_schema_path');
	register_setting('sbcd_as_settings', 'sbcd_as_api_activation');
}

/**
 * Déclaration de la page d'administration.
 *
 * @access public
 * @return void
 */
function sbcd_as_admin() {
	if ( !current_user_can( 'manage_options' ) )  {
		wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
	}
	include dirname(__file__).'/admin.php';
}

/**
 * Ajout du menu d'option dans les réglages
 *
 * @access public
 * @return void
 */
function sbcd_as_menu() {
	$hook = add_options_page( 'Apidae Structure', 'Apidae Structure', 'manage_options', 'sbcd-as-admin', 'sbcd_as_admin' );
}

/**
 * Mise en place des routes API et de l'appel au traitement de la notification d'export
 *
 * @access public
 * @return void
 */
function sbcd_as_api() {
	// Ajout de la route pour les notifications d'export
	register_rest_route( 'sbcd-as/v1', '/export/notification',
		array(
			'methods' 	=> 'POST',
			'callback' 	=> 'sbcd_as_setNotification',
			'permission_callback' => '__return_true'
		)
	);
	// Ajout de la route pour les téléchargements CSV
	register_rest_route( 'sbcd-as/v1', '/download/csv',
		array(
			'methods' 	=> 'GET',
			'callback' 	=> 'sbcd_as_downloadCSV',
			'permission_callback' => '__return_true'
		)
	);
	// Ajout de la route pour les téléchargements XML
	register_rest_route( 'sbcd-as/v1', '/download/xml',
		array(
			'methods' 	=> 'GET',
			'callback' 	=> 'sbcd_as_downloadXML',
			'permission_callback' => '__return_true'
		)
	);
	// Ajout des routes des API référentiel si les API sont activées.
	if (get_option('sbcd_as_api_activation') == true) {
		// Ajout de la route pour le référentiel communes
		register_rest_route( 'sbcd-as/v1', '/api/communes',
			array(
				'methods' 	=> 'GET',
				'callback' 	=> 'sbcd_api_communes',
				'permission_callback' => '__return_true'
			)
		);
		// Ajout de la route pour le référentiel territoires
		register_rest_route( 'sbcd-as/v1', '/api/territoires',
			array(
				'methods' 	=> 'GET',
				'callback' 	=> 'sbcd_api_territoires',
				'permission_callback' => '__return_true'
			)
		);
		// Ajout de la route pour le référentiel elements_reference
		register_rest_route( 'sbcd-as/v1', '/api/referentiel',
			array(
				'methods' 	=> 'GET',
				'callback' 	=> 'sbcd_api_elements_reference',
				'permission_callback' => '__return_true'
			)
		);
	}
}

/**
 * Renvoie un fichier JSON du référentiel demandé s'il existe.
 *
 * @access public
 * @param string $referentiel : communes, territoires, elements_reference
 * @return object JSON
 */
function sbcd_api_referentiel($referentiel) {
	// Récupération de l'espace de stockage du référentiel
	$referencePath = WP_CONTENT_DIR.'/uploads/sbcd_as/export/';

	$jsonPath = $referencePath.$referentiel.'.json';
	if (is_file($jsonPath)) {
		$content = json_decode(file_get_contents($jsonPath));
	}else{
		$content = array('status' => 'Error', 'message' => 'Reference file '.$referentiel.' not found.');
	}

	// Renvoi du fichier
	header('Access-Control-Allow-Origin: *');
	header('Content-type: application/json; charset=utf-8');
	header('Cache-Control: no-cache, must-revalidate');
	$json = fopen('php://output', 'w');
	fwrite($json, json_encode($content));
	fclose($json);
	exit;
}

/**
 * Renvoie le référentiel communes via l'API si le fichier JSON de l'export est présent.
 *
 * @access public
 * @return void
 */
function sbcd_api_communes() {
	// Renvoi le référentiel communes en JSON
	return sbcd_api_referentiel('communes');
}

/**
 * Renvoie le référentiel territoires via l'API si le fichier JSON de l'export est présent.
 *
 * @access public
 * @return void
 */
function sbcd_api_territoires() {
	// Renvoi le référentiel territoires en JSON
	return sbcd_api_referentiel('territoires');
}

/**
 * Renvoie le référentiel elements_reference via l'API si le fichier JSON de l'export est présent.
 *
 * @access public
 * @return void
 */
function sbcd_api_elements_reference() {
	// Renvoi le référentiel elements_reference en JSON
	return sbcd_api_referentiel('elements_reference');
}

/**
 * Appel de l'affichage de la structure d'un Type objet Apidae.
 *
 * @access public
 * @return String HTML
 */
function sbcd_run_structure() {
	// Vérifie si les paramètrages ont été faits
	if (get_option('sbcd_as_schema_path') =='') {
		return '<div class="sbcd_as"><div class="erreur">'.__('Le plugin <i>Apidae Structure</i> n\'est pas encore paramétré.').'</div></div>';
	}
	// Vérification de disponibilité de tous les fichiers nécessaires

	// Type objet non sélectionné : on affiche la liste des types objets
	if (!isset($_GET['sbcd_as_sch'])) {
		return sbcd_as_listTypes();
	// Si un type objet est sélectionné, on traite la demande.
	}else {
		require_once 'sbcd_apidae/sbcd_structure.php';
		require_once 'sbcd_apidae/sbcd_cache.php';
		$name = $_GET['sbcd_as_sch'];
		$schemaPath = WP_CONTENT_DIR.'/uploads/sbcd_as/schemas/';
		$referencePath = WP_CONTENT_DIR.'/uploads/sbcd_as/export/';
		$cachePath = WP_CONTENT_DIR.'/uploads/sbcd_as/structure/';
		$struct = new sbcd_structure($name,$schemaPath,$referencePath,$cachePath);

		$rtr = $struct->readStructure($_GET['sbcd_as_sch']);

		$rtr .= '<div class="sbcd_dwld">Télécharger
				<a href="'.esc_url( home_url( '/wp-json/sbcd-as/v1/download/xml' ) ).'/?sbcd_as_sch='.$name.'" target="_blank" class="sbcd_button">XML</a>
				<a href="'.esc_url( home_url( '/wp-json/sbcd-as/v1/download/csv' ) ).'/?sbcd_as_sch='.$name.'" target="_blank" class="sbcd_button">CSV</a>
				</div>';

		return '<div class="sbcd_as">'.$rtr.'</div>';
	}
}
?>