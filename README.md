# Apidae Structure

Ce plugin est destiné à calculer la structure des données du réseau d'information touristique Apidae, côté API et exports. 
Il offre les possibilités suivantes :

- Recevoir et décompresser les données d'un export Apidae au format JSON V2,
- Construire une représentation de la structure des données Apidae en croisant les schemas et le référentiel,
- Rendre disponible la structure des données au téléchargement en XML ou CSV,
- Ouvrir des API en lecture JSON pour les référentiels communes, territoires et éléments de référence.



## Pré-requis

- Nécessite un compte de membre et un projet numérique Apidae,
- Wordpress 4.4 ou plus pour permettre la mise en place des API REST,
- Extension ZIP pour la décompression des schémas et des exports,
- Extension XSL pour le calcul de la structure de données,
- Le WP-Cron de Wordpress doit être actif.



## Installation

Installer le plugin Wordpress comme un plugin habituel.

Pour cloner le répertoire Git, positionner un terminal dans le dossier plugins de WordPress et utiliser la commande suivante :
``git clone https://gitlab.com/Slt/apidae-structure.git apidae-sbcd-structure``

Configurer le plugin dans Réglages > Apidae Structure

#### Schémas JSON

Il faudra renseigner l'URL de l'archive des shémas API V2. 
L'URL est disponible sur la [page dédiée aux schémas](http://dev.apidae-tourisme.com/fr/documentation-technique/outils-developpeurs/schemas-json) dans la [documentation développeurs](http://dev.apidae-tourisme.com/).

Une fois l'URL du schéma renseignée, il faudra **enregistrer les modifications** pour voir apparaitre le bouton **importer le schéma**. L'import du schéma est manuel et nécessaire au calcul de la structure des données.

#### API

Les API permettent de renvoyer au format JSON V2 les référentiels communes, territoires et les éléments de référence.

Cette possibilité est activable ou desactivable en fonction des besoins.

#### Côté Apidae

Pour pouvoir calculer la structure des données et calculer la structure des données Apidae, le plugin a besoin d'utiliser les éléments de référence. Contrairement aux schémas JSON qui ne changent qu'assez rarement, les éléments de référence évoluent très régulièrement.

Pour que la structure des données soit à jour, il faudra utiliser un projet numérique sur Apdiae.

Les éléments de configuration technique du projet numérique vous sont donnés dans les réglages du plugin. La méthode de [création d'un projet numérique de diffusion](https://aide.apidae-tourisme.com/hc/fr/articles/360000828071-Cr%C3%A9er-son-projet-num%C3%A9rique) est disponible sur l'[aide en ligne](https://aide.apidae-tourisme.com).



## Fonctionnement du plugin

#### Récupération des schémas

Les schémas sont récupérés grace à l'adresse de l'archive donnée en paramètre dans les réglages. Au clic sur **Importer des schémas**, les schémas sont décompressés dans ``/wp-content/uploads/sbcd_as/shemas/``.

#### Récupération des exports

Le plugin met en place un webservice qui attend les notifications en provenance d'Apidae.

Lorsque le webservice reçoit une notification, il stocke les indications reçues dans un fichier ``/wp-content/uploads/sbcd_as/notifications.json``. 

Un cron est mis en place sur Wordpress toutes les heures. Si le fichier ``notifications.json`` est présent lors du passage du wp-cron, le plugin va le télécharger le fichier d'export Apidae, le décompresser dans ``/wp-content/uploads/sbcd_as/exports/``, effacer le fichier ``notifications.json`` puis prévenir Apidae que l'export s'est bien passé (si tout a bien fonctionné).

#### Calcul de la structure des données

La structure des données est ensuite calculée à partir des schémas JSON mis en regard des éléments de référence. La forme de la structure est dictée par le modèle XSLT présent dans le fichier ``apidae_structure.xsl``.

La structure de chaque type objet est systématiquement mise en cache après le premier calcul dans le dossier ``/wp-content/uploads/sbcd_as/structure/``. 
La durée du cache est d'une journée.

