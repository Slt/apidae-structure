<?php
class sbcd_cache {

  /**
   * Constructor
   *
   * @param string $dir
   */
  function __construct($dir = '.') {
    $this->dir = $dir;
    // Si le dossier de cache n'existe pas
    if (!is_dir($this->dir)) {
    	// On tente de le créer
    	@mkdir($dir, 0755, true) or die(__('Impossible de créer le dossier de cache '.$dir));
    }
  }

  /**
   * get pathname for $key (generated with sha1 from $key)
   *
   * @param string $key
   * @return string
   */
  private function _name($key) {
    return sprintf("%s/%s", $this->dir, sha1($key));
  }

  /**
   * get content from cache with key $key
   * invalid cache file if expirated
   *
   * @param string $key
   * @param int $expiration (0 for no expiration)
   * @return mixed bool or content from cache
   */
  public function get($key = 'default', $expiration = 3600) {
    // test if cache dir exists and writable
    if ( !is_dir($this->dir) || !is_writable($this->dir) ) {
      return false;
    }

    // test if cache file exists
    $cache_path = $this->_name($key);
    if (!@file_exists($cache_path)) {
      return false;
    }

    // test cache expiration (clear file if expired)
    if ($expiration != 0){
		if (filemtime($cache_path) < (time() - $expiration)) {
			$this->clear($key);
			return false;
		}
    }

    // test file readable
    if (!$fp = @fopen($cache_path, 'rb')) {
      return false;
    }

    // lock file and get cache file content
    flock($fp, LOCK_SH);
    $cache = '';
    if (filesize($cache_path) > 0) {
        $cache = unserialize(fread($fp, filesize($cache_path)));
    } else {
        $cache = NULL;
    }
    flock($fp, LOCK_UN);
    fclose($fp);

    return $cache;
  }

  /**
   * set cache content for key $key
   *
   * @param string $key
   * @param mixed $data
   * @return bool
   */
  public function set($key = 'default', $data = '') {
    if ( !is_dir($this->dir) || !is_writable($this->dir)) {
        return false;
    }

	$cache_path = $this->_name($key);
    if ( ! $fp = fopen($cache_path, 'wb')) {
      return false;
    }

    // lock file and set content
    if (flock($fp, LOCK_EX)) {
		fwrite($fp, serialize($data));
		flock($fp, LOCK_UN);
    } else {
      return false;
    }
    fclose($fp);
    @chmod($cache_path, 0777);
    return true;
  }

  /**
   * clear cache for key $key
   *
   * @param string $key
   * @return bool
   */
  public function clear($key = 'default')
  {
    $cache_path = $this->_name($key);
    if (file_exists($cache_path))
    {
      unlink($cache_path);
      return true;
    }

    return false;
  }
}
