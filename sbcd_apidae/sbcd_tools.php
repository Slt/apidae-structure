<?php
class sbcd_tools {
	/**
	 *
	 * Ecrit un fichier en le lockant et attribuant éventuellement un chmod
	 * @param string $path
	 * @param string $content
	 * @param numeric $chmod
	 * @return bool true/false
	 */
	public static function writeFile($path,$content,$chmod=0755) {
	    if ( ! $fp = fopen($path, 'wb')) {
	      return false;
	    }

	    // lock file and set content
	    if (flock($fp, LOCK_EX)) {
	      fwrite($fp, $content);
	      flock($fp, LOCK_UN);
	    } else {
	      return false;
	    }
	    fclose($fp);
	    @chmod($path, $chmod);
	    return true;
	}

	/**
	 *
	 * Lit un fichier en le lockant au préalable.
	 * @param string $path to file
	 * @return string. Content of file.
	 */
	public static function readFile($path) {
		// test file readable
		if (!$fp = @fopen($path, 'rb')) {
			return false;
		}

		// lock file and get cache file content
		flock($fp, LOCK_SH);
		$content = '';
		if (filesize($path) > 0) {
			$content = fread($fp, filesize($path));
		} else {
			$content = NULL;
		}
		flock($fp, LOCK_UN);
		fclose($fp);

		return $content;
	}

	/**
	 * Télécharge un fichier et le stocke dans le dossier de destination.
	 *
	 * @access public
	 * @param mixed $url
	 * @param mixed $destination
	 * @return bool
	 */
	public static function downloadFile($url,$destination) {
		$handle = fopen($url, "rb");
		if ($contents = stream_get_contents($handle)) {
			$handlew = fopen($destination, "w");
			fwrite($handlew, $contents);
			fclose($handlew);
			return true;
		}
		return false;
	}

	/**
	 *
	 * Dezippe un fichier dans un dossier.
	 * @param string $file
	 * @param string $destinationPath
	 */
	public static function unzip($file,$destinationPath) {
		if (!extension_loaded('zip')) {
			sprintf(__( 'The zip extension must be activated in PHP.'),$path);
			return false;
		}
		$zip = new ZipArchive;
		$res = $zip->open($file);
		if ($res === TRUE) {
		  $zip->extractTo($destinationPath);
		  $zip->close();
		  return true;
		} else {
		  return false;
		}
	}

	/**
	 * Effacement d'un dossier contenant ou non des fichiers
	 * @param string $dirPath
	 */
	public static function safe_rmdir($dirPath) {
	    foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($dirPath, FilesystemIterator::SKIP_DOTS), RecursiveIteratorIterator::CHILD_FIRST) as $path) {
	        $pathName = $path->getPathname();
	        ($path->isDir() and ($path->isLink() === false)) ? rmdir($pathName) : unlink($pathName);
	    }
	    rmdir($dirPath);
	    return true;
	}

	/**
	 * Création d'un dossier.
	 *
	 * @access public
	 * @param mixed $path
	 * @return void
	 */
	public static function safe_mkdir($path) {
		if (!is_dir($path)) {
			if (!mkdir($path)) {
				wp_die( sprintf(__( 'The temporary folder cannot be created. Make sure the %s is writable.'),$path) );
			}
		}
		return true;
	}
}
?>