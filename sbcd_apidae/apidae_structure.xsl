<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="xml" omit-xml-declaration="yes"/>

	<xsl:template match="/">
		<h2>
			<xsl:value-of select="structure/groupe/critere/@id" />
		</h2>
		<div id="sit">
			<ul>
				<xsl:apply-templates select="structure/groupe"></xsl:apply-templates>
			</ul>
		</div>
	</xsl:template>

	<xsl:template match="groupe">
		<xsl:if test="not(contains(@id, 'Add') and not(substring-after(@id, 'Add')))">
			<xsl:if test="not(contains(@id, 'Remove') and not(substring-after(@id, 'Remove')))">
				<li>
					<span class="nom">
						<xsl:if test="count(groupe) > 0 or count(critere) > 0 ">
							<xsl:attribute name="class">
								<xsl:text>nom slidable</xsl:text>
							</xsl:attribute>
						</xsl:if>
						<xsl:value-of select="@id" />
					</span>
					(<xsl:value-of select="@type" />)
					<xsl:if test="count(groupe) > 0">
						<ul style="display:none;">
							<xsl:apply-templates select="groupe"></xsl:apply-templates>
						</ul>
					</xsl:if>

					<xsl:if test="count(critere) > 0">
						<ul style="display:none;">
							<xsl:apply-templates select="critere"></xsl:apply-templates>
						</ul>
					</xsl:if>
				</li>
			</xsl:if>
		</xsl:if>
	</xsl:template>

	<xsl:template match="critere">
		<li>
			<xsl:if test="@actif=0">
				<xsl:attribute name="class">
					<xsl:text>unused</xsl:text>
				</xsl:attribute>
			</xsl:if>
			<span class="nom">
				<xsl:if test="@code!=''">
					<xsl:value-of select="@code" />-
				</xsl:if>
				<xsl:value-of select="normalize-space(@id)" />
			</span>
		</li>
	</xsl:template>

</xsl:stylesheet>