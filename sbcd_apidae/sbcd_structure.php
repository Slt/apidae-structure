<?php
// require_once 'sbcd_cache.php';

class sbcd_structure {
	var $name;
	var $schema;
	var $listes;
	var $structure;

	private $elementReferencePath;	// Chemin vers le fichier elements_reference.json
	private $cachePath;				// Chemin vers le dossier utilisé pour stocker les structures calculées
	private $schemaPath;			// Chemin vers le dossier contenant les schémas Json-V2

	/**
	 * Constructeur de la structure
	 *
	 * @access public
	 * @param mixed $name			Nom du schéma Apidae
	 * @param mixed $schemaPath		Emplacement du dossier contenant les schémas Json-V2
	 * @param mixed $referencePath	Emplacement du dossier contenant le ficheir elements_reference.json
	 * @param mixed $cachePath		Emplacement du dossier utilisé pour stocker les structures calculées
	 * @return bool 				true / false
	 */
	public function __construct($name, $schemaPath, $referencePath, $cachePath) {
		$this->elementReferencePath = str_replace('//','/',$referencePath.'/elements_reference.json');
		$this->cachePath = str_replace('//','/',$cachePath.'/');
		$this->schemaPath = str_replace('//','/',$schemaPath.'/');
		$this->name = $name;

		if ($this->loadStructure($name)) {
			return true;
		} else {
			// Vérification de la présence des fichiers nécessaires
			if ($diag = $this->isFilesProblems($name)) {
				foreach($diag as $erreur) {
					echo $erreur;
				}
				return false;
			}
			$this->loadListes();
			$this->loadSchema($name);
			$this->structure = new SimpleXMLElement('<structure />');

			// Lancer une analyse
			$this->structure = $this->parse($this->schema, $this->structure);

			$this->saveStructure();
			return true;
		}
		return false;
	}

	/**
	 * Tableau de correspondance des types objets entre la version intelligible et l'ID Apidae.
	 *
	 * @access public
	 * @static
	 * @return array
	 */
	public static function listTypesObjets() {
		$schemas = array(
			//	Libelle					=>	ID
			'Restauration'				=>	'RESTAURATION',
			'Patrimoine naturel'		=>	'PATRIMOINE_NATUREL',
			'Patrimoine culturel'		=>	'PATRIMOINE_CULTUREL',
			'Hôtellerie'				=>	'HOTELLERIE',
			'Hôtellerie de plein air'	=>	'HOTELLERIE_PLEIN_AIR',
			'Hébergement locatif'		=>	'HEBERGEMENT_LOCATIF',
			'Hébergement collectif'		=>	'HEBERGEMENT_COLLECTIF',
			'Fête et manifestation'		=>	'FETE_ET_MANIFESTATION',
			'Equipement'				=>	'EQUIPEMENT',
			'Domaine skiable'			=>	'DOMAINE_SKIABLE',
			'Commerce et service'		=>	'COMMERCE_ET_SERVICE',
			'Activité'					=>	'ACTIVITE',
			'Dégustation'				=>	'DEGUSTATION',
			'Territoire'				=>	'TERRITOIRE',
			'Entité juridique'			=>	'STRUCTURE',
			'Séjour'					=>	'SEJOUR_PACKAGE'
		);
		return $schemas;
	}

	/**
	 * Verification de la présence des fichiers essentiels au traitement.
	 *
	 * @access public
	 * @param mixed $name
	 * @return array width alert messages or TRUE
	 */
	public function isFilesProblems($name) {
		$diag = [];
		// Vérification de la présence de l'export
		if (!is_file($this->elementReferencePath)) {
			$diag[] = __('Fichier d\'export JSON V2 absent.<br />');
		}

		// Vérification de la présence des schémas
		$path = $this->schemaPath.'objetTouristique/'.$name.'.schema';
		if (!is_file($path)) {
			$diag[] = __('Le schema JSON "'.$name.'" est absent.');
		}

		if (!empty($diag)) {
			return $diag;
		}
		return false;
	}

	/**
	 * Chargement de la structure en cache. Régénération au bout d'une journée
	 *
	 * @access public
	 * @param mixed $name
	 * @return bool True / False
	 */
	public function loadStructure($name){
		// Chargement du cache
		$cache = new sbcd_cache($this->cachePath);
		if ($structure = $cache->get($name,86400)) { // Cache à 1 jour.
			if ($this->structure = simplexml_load_string($structure)) { // Cache à 1 jour.
				return true;
			}
		}
		return false;
	}

	/**
	 * Enregistrement de la structure dans le dossier de cache.
	 *
	 * @access public
	 * @return bool True / False
	 */
	public function saveStructure(){
		$cache = new sbcd_cache($this->cachePath);
		$xml = $this->structure->asXML();
		if ($cache->set($this->name,$xml)){
			return true;
		}
		return false;
	}

	/**
	 * Charge un schéma en fonction de son nom de fichier (sans l'extension).
	 *
	 * @access public
	 * @param string $name
	 * @return object json
	 */
	public function loadSchema($name) {
		$path = $this->schemaPath.'objetTouristique/'.$name.'.schema';
		$json_f = file_get_contents($path);
		$json = json_decode($json_f);
		return $this->schema = $json;
	}

	/**
	 * Renvoie les éléments de référence organisés par noms de listes
	 * Après les avoir récupéré dans le cache ou recalculé.
	 *
	 * @access public
	 * @return array of objects
	 * @todo dev.
	 */
	public function loadListes() {
		// Chargement du cache
		$cache = new sbcd_cache($this->cachePath);
		$listes = $cache->get('listesApidae',86400); // Cache à 1 jour.
		// Sinon initRefListes
		if (!is_array($listes)){
			$listes = $this->initRefListes();
			// Enregistrement du cache si possible
			$cache->set('listesApidae',$listes);
		}
		// Renvoi des listes
		return $this->listes = $listes;
	}

	/**
	 * Charge la liste les éléments de référence et les organise par noms de listes.
	 *
	 * @access public
	 * @return array
	 */
	public function initRefListes() {
		// Chargement des éléments de référence
		$refListes_f = file_get_contents($this->elementReferencePath);
		$refListes = json_decode($refListes_f);

		// Si la liste n'a pas encore été convertie.
		if (isset($refListes[0]) && is_object($refListes[0])) {
			$listes = array();
			// chargement des éléments utiles des listes
			foreach ($refListes as $item) {
				// Nettoyage du tableau initial
				$critere = array();
				// Charge les valeurs
				$critere['id']												= $item->id;
				$critere['liste']											= $item->elementReferenceType;
				$critere['libelleFr']										= $item->libelleFr;
				if (isset($item->libelleEn)) 	{ $critere['libelleEn'] 	= $item->libelleEn; }
				if (isset($item->libelleEs))	{ $critere['libelleEs'] 	= $item->libelleEs; }
				if (isset($item->libelleIt)) 	{ $critere['libelleIt'] 	= $item->libelleIt; }
				if (isset($item->libelleDe)) 	{ $critere['libelleDe'] 	= $item->libelleDe; }
				if (isset($item->libelleNl)) 	{ $critere['libelleNl'] 	= $item->libelleNl; }
				$critere['ordre']											= $item->ordre;
				$critere['actif']											= $item->actif;
				if (isset($item->description)) 	{ $critere['description'] 	= $item->description; }

				// Ajoute le tableau du critère à la liste en cours.
				$listes[$item->elementReferenceType][] = $critere;
			}

			// Ré-ordonnancement des listes par ordre
			foreach ($listes as $liste) {
				$ordre = array();
				foreach($liste as $k => $v) {
					$ordre[$k] = $v['ordre'];
				}
				array_multisort($ordre, SORT_ASC, $liste);
				$listes[$liste[0]['liste']] = $liste;
			}
			return $listes;
		}else{
			return $refListes;
		}
	}

	/**
	 * Renvoie le type de l'entrée.
	 *
	 * @access public
	 * @static
	 * @param mixed $var
	 * @return string
	 */
	public static function type($var) {
		if (is_string($var)) {
			return 'string';
		}
		elseif(is_bool($var)) {
			return 'bool';
		}
		elseif(is_float($var)) {
			return 'float';
		}
		elseif(is_int($var)) {
			return 'var';
		}
		elseif(is_array($var)) {
			return 'array';
		}
		elseif(is_object($var)){
			return 'object';
		}
		return false;
	}

	/**
	 * Parcours de la structure et remplissage du fichier de structure en XML.
	 *
	 * @access public
	 * @param object $json
	 * @param object $struct
	 * @param bool $noLine (default: false)
	 * @return object
	 */
	public function parse($json, $struct, $noLine=false) {
		foreach ($json as $key => $entry) {
			$type = null; $clef = null; $data = null;
			$type = self::type($entry);
			$clef = strval($key);

			if ($type == 'object') {
				// Si le noeud enfant est un élément de référence, on saute une profondeur.
				if (isset($entry->id) && isset($entry->properties->elementReferenceType)) {
					$groupe = $struct->addChild('groupe');
					$groupe->addAttribute('id',$entry->id);
					$groupe->addAttribute('type',$this->getChamp($entry->properties->elementReferenceType));
					$this->getList($entry->properties->elementReferenceType->enum[0],$groupe);
					// Parcours des niveaux inférieurs
					$this->parse($entry->properties->elementReferenceType, $groupe);
				} else {
					switch ($clef) {
						case 'parent' :
							break;
						case 'familleCritere' :
							break;
						case 'properties' :
							$this->parse($entry, $struct, true);
							break;
						default :
							$groupe = $struct->addChild('groupe');
							$groupe->addAttribute('id',$clef);
							$groupe->addAttribute('type',strtolower($this->getChamp($entry)));
							if ($this->getChamp($entry) == 'Liste de choix') {
								$this->getChoix($entry, $groupe);
							}
							// Parcours des niveaux inférieurs
							$this->parse($entry, $groupe);
					}
				}
			}
		}
		return $struct;
	}

	/**
	 * Retrouve le libellé d'un critère en fonction de son identifiant.
	 *
	 * @access public
	 * @param mixed $data
	 * @param mixed $struct
	 * @return void
	 */
	public function getChoix($data, $struct) {
		if (is_object($data)) {
			$data = $data->enum;
		}
		$arrRtr = null;
		natsort($data);
		foreach($data as $e) {
			$item = $struct->addChild('critere');
			$item->addAttribute('id',$e);
			$item->addAttribute('type','choix');

			// ajout liste
			$this->getList($e,$item);
		}
		return false;
	}

	/**
	 * Retrouve la liste en fonction du nom.
	 *
	 * @access public
	 * @param mixed $nomListe
	 * @return HTML
	 */
	public function getList($nomListe, $struct) {
		if (isset($this->listes[$nomListe])) {
			foreach ($this->listes[$nomListe] as $item) {
				$critere = $struct->addChild('critere');
				$critere->addAttribute('id',$item['libelleFr']);
				$critere->addAttribute('code',$item['id']);
				if (isset($item['libelleEn'])) { $critere->addAttribute('libelleEn',$item['libelleEn']); }
				if (isset($item['libelleEs'])) { $critere->addAttribute('libelleEs',$item['libelleEs']); }
				if (isset($item['libelleIt'])) { $critere->addAttribute('libelleIt',$item['libelleIt']); }
				if (isset($item['libelleDe'])) { $critere->addAttribute('libelleDe',$item['libelleDe']); }
				if (isset($item['libelleNl'])) { $critere->addAttribute('libelleNl',$item['libelleNl']); }
//				if (isset($item['description'])) { $critere->addAttribute('description',$item['description']); }
				$item['actif'] == 1 ? $actif = 1 : $actif = 0;
				$critere->addAttribute('actif',$actif);
				$critere->addAttribute('type','liste');
			}
			return true;
		}
		return false;
	}

	/**
	 * Renvoi le type de champ en fonction de sa structure.
	 *
	 * @access public
	 * @param object $json
	 * @return string
	 */
	public function getChamp($json) {
		if (isset($json->type)) {
			if($json->type == 'string') {
				if (isset($json->enum)) {
					return 'Liste de choix';
				}
				if (isset($json->format) && strval($json->format) == 'date-time') {
					return 'Date et heure';
				}
				if (strval($json->type == 'string')) {
					return 'Champ texte';
				}
			}elseif (isset($json->type) && strval($json->type == 'integer')) {
				return 'Champ numerique';
			}elseif (isset($json->type) && strval($json->type == 'boolean')) {
				return 'Booléen';
			}
		}
		return 'Granule';
	}

	/**
	 *
	 * Fonction de recursion sur les differents noeuds.
	 * @param object $xml
	 * @param string $typeChamp
	 */
	public function parseXML($xml, $fp, $obj, $cnt=0) {
		$name = null;
		if (isset($obj->name)) {
			$name = $obj->name;
		}
		foreach ($xml as $x) {
			$ligne = array();
			// si on est sur un champ ou un item on passe en revue
			$typObj = $x[0]['type'];

			if ($typObj != 'granule' && $typObj != 'liste de choix' ) {
				$libelle = $x[0]['id'];
				// Retrouve le chemin et le nom du bordereau.
				$chemin =  null;
				$chemin = substr(strval($this->getCheminFromThere($x)),0,-1);
				if (empty($chemin)) {
					$chemin = $libelle;
					$libelle = '';
				}

				// La ligne sera composee comme suit
				$ligne[] = $cnt;
				$ligne[] = utf8_decode($name);
				$ligne[] = utf8_decode($chemin);
				$ligne[] = utf8_decode($typObj);
				if (isset($x[0]['actif']) && ($x[0]['actif']==0)) {
					$ligne[] = 'Inactif';
				}else{
					$ligne[] = '';
				}
				$ligne[] = utf8_decode($x[0]['code']);
				$ligne[] = utf8_decode($libelle);
				$ligne[] = utf8_decode($x[0]['libelleEn']);
				$ligne[] = utf8_decode($x[0]['libelleEs']);
				$ligne[] = utf8_decode($x[0]['libelleIt']);
				$ligne[] = utf8_decode($x[0]['libelleDe']);
				$ligne[] = utf8_decode($x[0]['libelleNl']);

				// On place la ligne dans le fichier
				fputcsv($fp, $ligne, ';');
				$cnt++;

			}
			// S'il reste des noeuds enfants, on les parcours.
			if ($x->children()) {
				$this->parseXML($x->children(), $fp, $obj, $cnt);
			}
	/*
			// Limiteur anti-vautre !
			if ($cnt > 90){
				break;
			}
	*/
		}
	}

	/**
	 * Affiche l'arbre correspondant à la structure du type d'objet.
	 *
	 * @access public
	 * @param mixed $plug
	 * @return void
	 */
	function readStructure($name) {
		$cacheName = 'html'.$name;

		$cache = new sbcd_cache($this->cachePath);
		if (!$render = $cache->get($cacheName,86400)) { // Cache à 1 jour.
			// Erreur si tous les éléments nécessaires ne sont pas présents.
			if ($this->isFilesProblems($name)) {
				return false;
			}
			$xmlData = $this->structure->asXML();
			$xslPath = realpath(dirname(__FILE__).'/apidae_structure.xsl');

			$xml = new DomDocument;
			$xml->loadXML($xmlData);

			$xsl = new DomDocument;
			$xsl->load($xslPath);

			$xslt = new xsltProcessor;
			$xslt->importStyleSheet($xsl);
			$render = $xslt->transformToXML($xml);

			$cache->set($cacheName,$render);
		}
		return $render;
	}

	/**
	 * Renvoi le chemin parent d'un noeud XML.
	 *
	 * @access public
	 * @param mixed $xml
	 * @param mixed $chemin (default: null)
	 * @return void
	 */
	public function getCheminFromThere($xml,$chemin=null) {
		$parent = $xml->xPath('..');
		if(isset($parent[0]['id'])) {
			$chemin = trim($parent[0]['id']).'/'.$chemin;
			return $this->getCheminFromThere($parent[0],$chemin);
		}else{
			return $chemin;
		}
	}
}
?>